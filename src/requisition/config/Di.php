<?php

use people_sdk\file\requisition\request\info\factory\model\FileConfigSndInfoFactory;



return array(
    // File requisition request sending information services
    // ******************************************************************************

    'people_file_requisition_request_snd_info_factory' => [
        'source' => FileConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/file/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);