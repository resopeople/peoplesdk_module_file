<?php

use people_sdk\file\requisition\request\info\factory\model\FileConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_file_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'file' => [
            'requisition' => [
                'request' => [
                    // File requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see FileConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'file_support_type' => 'header',
                            'file_file_content_include_config_key' => 'people_file_file_content_include'
                        ]
                    ]
                ]
            ]
        ]
    ]
);