<?php
/**
 * Description :
 * This class allows to define group member module bootstrap class.
 * Group member module bootstrap allows to boot group member module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_file\group_member\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\file\group\member\model\GrpMemberEntityFactory;
use people_sdk\module_file\group_member\library\ConstGrpMember;



class GrpMemberBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Group member entity factory instance.
     * @var GrpMemberEntityFactory
     */
    protected $objGrpMemberEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param GrpMemberEntityFactory $objGrpMemberEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        GrpMemberEntityFactory $objGrpMemberEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objGrpMemberEntityFactory = $objGrpMemberEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstGrpMember::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set group member entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'file', 'group_member', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objGrpMemberEntityFactory->getTabConfig(), $tabConfig);
            $this->objGrpMemberEntityFactory->setTabConfig($tabConfig);
        };
    }



}