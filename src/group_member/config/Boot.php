<?php

use people_sdk\module_file\group_member\boot\GrpMemberBootstrap;



return array(
    'people_file_group_bootstrap' => [
        'call' => [
            'class_path_pattern' => GrpMemberBootstrap::class . ':boot'
        ]
    ]
);