<?php

use people_sdk\file\group\member\model\GrpMemberEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'file' => [
            'group_member' => [
                // Group member entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see GrpMemberEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Group entity factory execution configuration array format:
                     * @see GrpMemberEntityFactory::setTabGroupEntityFactoryExecConfig() configuration format.
                     */
                    'group_factory_execution_config' => [],

                    /**
                     * File entity factory execution configuration array format:
                     * @see GrpMemberEntityFactory::setTabFileEntityFactoryExecConfig() configuration format.
                     */
                    'file_factory_execution_config' => []
                ]
            ]
        ]
    ]
);