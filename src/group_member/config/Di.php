<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\group\member\model\GrpMemberEntityCollection;
use people_sdk\file\group\member\model\GrpMemberEntityFactory;
use people_sdk\file\group\member\model\repository\GrpMemberEntityMultiRepository;
use people_sdk\file\group\member\model\repository\GrpMemberEntityMultiCollectionRepository;



return array(
    // Group member entity services
    // ******************************************************************************

    'people_file_group_member_entity_collection' => [
        'source' => GrpMemberEntityCollection::class
    ],

    'people_file_group_member_entity_factory_collection' => [
        'source' => GrpMemberEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_group_member_entity_factory' => [
        'source' => GrpMemberEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_file_group_member_entity_factory_collection'],
            ['type' => 'class', 'value' => GroupEntityFactory::class],
            ['type' => 'class', 'value' => FileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/file/group_member/factory/group_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/file/group_member/factory/file_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_group_member_entity_multi_repository' => [
        'source' => GrpMemberEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_file_group_member_entity_multi_collection_repository' => [
        'source' => GrpMemberEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_file_group_member_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_file_group_member_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    GrpMemberEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_file_group_member_entity_collection']
    ]
);