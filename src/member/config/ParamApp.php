<?php

use people_sdk\file\member\model\MemberEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'file' => [
            'member' => [
                // Member entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see MemberEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * User profile entity factory execution configuration array format:
                     * @see MemberEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                     */
                    'user_profile_factory_execution_config' => [],

                    /**
                     * File entity factory execution configuration array format:
                     * @see MemberEntityFactory::setTabFileEntityFactoryExecConfig() configuration format.
                     */
                    'file_factory_execution_config' => []
                ]
            ]
        ]
    ]
);