<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\member\model\MemberEntityCollection;
use people_sdk\file\member\model\MemberEntityFactory;
use people_sdk\file\member\model\repository\MemberEntityMultiRepository;
use people_sdk\file\member\model\repository\MemberEntityMultiCollectionRepository;



return array(
    // Member entity services
    // ******************************************************************************

    'people_file_member_entity_collection' => [
        'source' => MemberEntityCollection::class
    ],

    'people_file_member_entity_factory_collection' => [
        'source' => MemberEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_member_entity_factory' => [
        'source' => MemberEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_file_member_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'class', 'value' => FileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/file/member/factory/user_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/file/member/factory/file_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_member_entity_multi_repository' => [
        'source' => MemberEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_file_member_entity_multi_collection_repository' => [
        'source' => MemberEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_file_member_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_file_member_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    MemberEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_file_member_entity_collection']
    ]
);