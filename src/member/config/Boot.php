<?php

use people_sdk\module_file\member\boot\MemberBootstrap;



return array(
    'people_file_member_bootstrap' => [
        'call' => [
            'class_path_pattern' => MemberBootstrap::class . ':boot'
        ]
    ]
);