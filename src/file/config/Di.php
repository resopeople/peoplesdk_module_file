<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\file\file\model\FileEntityCollection;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\file\model\repository\FileEntityMultiRepository;
use people_sdk\file\file\model\repository\FileEntityMultiCollectionRepository;



return array(
    // File entity services
    // ******************************************************************************

    'people_file_entity_collection' => [
        'source' => FileEntityCollection::class
    ],

    'people_file_entity_factory_collection' => [
        'source' => FileEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_entity_factory' => [
        'source' => FileEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_file_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'class', 'value' => AppProfileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/file/factory/user_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/file/factory/app_profile_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_file_entity_multi_repository' => [
        'source' => FileEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'class', 'value' => FileFactoryInterface::class],
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class],
            ['type' => 'config', 'value' => 'people/file/repository/boundary']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_file_entity_multi_collection_repository' => [
        'source' => FileEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_file_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_file_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    FileEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_file_entity_collection']
    ]
);