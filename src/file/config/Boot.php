<?php

use people_sdk\module_file\file\boot\FileBootstrap;



return array(
    'people_file_bootstrap' => [
        'call' => [
            'class_path_pattern' => FileBootstrap::class . ':boot'
        ]
    ]
);