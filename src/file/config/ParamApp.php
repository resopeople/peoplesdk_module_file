<?php

use people_sdk\file\file\model\FileEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'file' => [
            // File entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see FileEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ],

                /**
                 * User profile entity factory execution configuration array format:
                 * @see FileEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                 */
                'user_profile_factory_execution_config' => [],

                /**
                 * Application profile entity factory execution configuration array format:
                 * @see FileEntityFactory::setTabAppProfileEntityFactoryExecConfig() configuration format.
                 */
                'app_profile_factory_execution_config' => []
            ],

            // File entity repository
            'repository' => [
                /**
                 * Boundary format:
                 * @var string
                 */
                'boundary' => '',
            ],
        ]
    ]
);