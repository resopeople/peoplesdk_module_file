<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/file/library/ConstFile.php');
include($strRootPath . '/src/file/boot/FileBootstrap.php');

include($strRootPath . '/src/member/library/ConstMember.php');
include($strRootPath . '/src/member/boot/MemberBootstrap.php');

include($strRootPath . '/src/group_member/library/ConstGrpMember.php');
include($strRootPath . '/src/group_member/boot/GrpMemberBootstrap.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');