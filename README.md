PeopleSdk_Module_File
=====================



Description
-----------

Library contains application modules,
to implements file components,
to use API file features, on application.
Application is considered as LibertyCode application.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework module installation requirement
-----------------------------------------

1. Module repository: liberty_code_module/validation: version 1.0

    - Module validation (or equivalent).
    - Module rule (or equivalent).

2. Module repository: liberty_code_module/datetime: version 1.0

    - Module datetime (or equivalent).
        
3. Module repository: people_sdk/module_library: version 1.0

    - Module requisition (or equivalent).

4. Module repository: people_sdk/module_user_profile: version 1.0

    - Module user (or equivalent).

5. Module repository: people_sdk/module_app_profile: version 1.0

    - Module app (or equivalent).

6. Module repository: people_sdk/module_group: version 1.0

    - Module group (or equivalent).

7. Other module implementation:

    - DI configuration: 
        
        - people_requisition_persistor:
        
            See people_sdk/file v1.0 framework library implementation requirement, 
            for persistor.
            
        - people_requisition_requester:
            
            See people_sdk/file v1.0 framework library implementation requirement, 
            for requester.
            
        - people_requisition_config.
        
---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root directory
    
    ```sh
    cd "<project_root_dir_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require people_sdk/module_file ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_dir_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_dir_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "people_sdk/module_file": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root directory.
    
2. Include source
    
    ```php
    require_once('<repository_root_dir_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project_root_dir_path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/people_sdk/module_file/src/file",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/people_sdk/module_file/src/member",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/people_sdk/module_file/src/group_member",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/people_sdk/module_file/src/requisition",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        }
    ]
    ```

---



Configuration
-------------

#### Application parameters configuration

- Use following file on your modules to configure specific elements
    
    ```sh
    <module_root_path>/config/ParamApp.php
    ```

- Elements configurables

    - Configuration to param file factory.
    
    - Configuration to param member factory.
    
    - Configuration to param group member factory.
    
    - Configuration to param file requisition request sending information factory.

---



Usage
-----

TODO

---


